import { Component, h } from '@stencil/core';

@Component({
  tag: 'app-root',
  styleUrl: 'app-root.css'
})
export class AppRoot {
	private async onRouteWillChange() {
		const loading = document.createElement('ion-loading');
	  loading.duration = 500;
		document.body.appendChild(loading);
		await loading.present();	
	}

  private async onRouteDidChange() {
		const loading = document.getElementsByTagName('ion-loading');
		try {
			for (let i =0 ; i < loading.length; i++) {
				loading[i].dismiss();
			}
		} catch (e) {
			console.debug('route change remove loading', e);
		}
	}


  render() {
    return (
      <ion-app>
        <ion-router
          onIonRouteWillChange={this.onRouteWillChange}
          onIonRouteDidChange={this.onRouteDidChange}
          useHash={false}
        >          
					<ion-route url="/" component="app-home" />
          <ion-route url="/profile/:name" component="app-profile" />
        </ion-router>
        <ion-nav />
      </ion-app>
    );
  }
}
