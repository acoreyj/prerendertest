import { Config } from '@stencil/core';

// https://stenciljs.com/docs/config

export const config: Config = {
  outputTargets: [{
    type: 'www',
		serviceWorker: null,
		baseUrl: "https://jolly-nobel-51a4e3.netlify.com"
  }],
  globalScript: 'src/global/app.ts',
  globalStyle: 'src/global/app.css'
};
